package com.vipbbo.security.springmvc.init;

import com.vipbbo.security.springmvc.config.ApplicationConfig;
import com.vipbbo.security.springmvc.config.WebConfig;
import com.vipbbo.security.springmvc.config.WebSecurityConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @author bbo(zbw)
 * Description 加载 Spring容器  此类实现WebApplicationInitializer接口，
 * Spring容器启动时加载WebApplicationInitializer接口的所有实现类
 * @Date 2021/4/21
 */
public class SpringApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    // spring 容器 相当于加载applicationContext.xml
    @Override
    protected Class<?>[] getRootConfigClasses() {
//        return new Class[0];
        return new Class[]{ApplicationConfig.class, WebSecurityConfig.class};
    }


    // servletContext  相当于加载springmvc.xml
    @Override
    protected Class<?>[] getServletConfigClasses() {
//        return new Class[0];
        return new Class[]{WebConfig.class};
    }

    // url-mapping
    @Override
    protected String[] getServletMappings() {
//        return new String[0];
        return new String[]{"/"};
    }
}
