package com.vipbbo.security.springmvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author bbo(zbw)
 * Description 登录Controller，对/login请求处理，它调用AuthenticationService完成认证并返回登录结果提示信息：
 * @Date 2021/4/22
 */

@RestController
public class LoginController {

    @RequestMapping(value = "/login-success", produces = {"application/json;", "text/html;charset=UTF-8;"})
    public String loginSuccess() {
        return "login success";
    }

    /*
    测试资源1
     */
    @GetMapping(value = "/r/r1", produces = {"application/json;", "text/html;charset=UTF-8;"})
    public String r1() {
        return "访问资源1";
    }

    /*
       测试资源2
     */
    @GetMapping(value = "/r/r2", produces = {"application/json;", "text/html;charset=UTF-8;"})
    public String r2() {
        return "访问资源2";
    }
}