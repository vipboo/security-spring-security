package com.vipbbo.security.springmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * @author bbo(zbw)
 * Description Spring 容器配置   它对应web.xml中ContextLoaderListener的配置
 * @Date 2021/4/22
 */
@Configuration
@ComponentScan(basePackages="com.vipbbo.security.springmvc"
        ,excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION
        ,value = Controller.class)})
public class ApplicationConfig  {
    //在此配置除了Controller的其它bean，比如：数据库链接池、事务管理器、业务bean等。
}
